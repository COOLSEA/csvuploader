

class Product < ActiveRecord::Base

   

   def self.import(file)
      allowed_attributes = ["id","name","released_on","price","create_at","updated_at"]
      spreadsheet = open_spreadsheet(file)
      header = spreadsheet.row(1)
      (2..spreadsheet.last_row).each do |i|
         row=Hash[[header,spreadsheet.row(i)].transpose]
         product = find_by_id(row["id"]) || new
         product.attributes = row.to_hash.select {|k,v| allowed_attributes.include? k }
         product.save!
      end
   end

   def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::Csv.new(file.path)
      when '.xls' then Roo::Excel.new(file.path, nil, :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path, nil, :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
   end


  def self.to_csv(options = {})
    CSV.generate(options) do |csv|

      #這一行是標題行
      csv << column_names
      all.each do |product|
        csv << product.attributes.values_at(*column_names)
      end
    end
  end



end
