class ProductsController < ApplicationController

  def index
    @products = Product.order(:name)
    respond_to do |format|
      format.html
      format.csv { send_data @products.to_csv }
      format.xls { send_data @products.to_csv(col_sep: "\t") }
    end
  end


	def import
		Product.import(params[:file])
		redirect_to root_url, noice: "Products imported."
	end


	private

	#rails4 之後的安全設定
    def product_params
      params.require(:product).permit(:name,:price,:released_on)
    end


end
