###這是一個可以匯入csv, xls, xlsx的程式


#####目前有以下功能
1. 可以使用csv格式匯入資料
2. 可以使用xls和xlsx格式匯入資料
3. 可以將資料下載成xls和csv格式


#####待辦清單
(https://github.com/coolsea/CSVuploader/blob/master/todo.todo)


######參考資料
[Importing CSV and Excel](http://goo.gl/9mFD1W)

